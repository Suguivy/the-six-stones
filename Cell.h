#ifndef CELL_H
#define CELL_H

#include "Creature.h"
#include "Item.h"
#include <list>

// This enum defines the different types of cells that exists
enum class CellType : char {
   SOLID, FREE 
};

class Cell {
public:
    Cell(CellType type);
    ~Cell();
    char get_representation(); // Returs the char representation of the cell
    void set_creature(Creature* creature); // Sets the creature that the cell contains
private:
    CellType m_type; // Type of the cell (free, solid, trap...)
    Creature* m_creature; // Creature on the cell, or nullptr if no creature
    std::list<Item*> m_items; // List of items that contains the cell
    
};

#endif // CELL_H
