#ifndef GAME_H
#define GAME_H

#include "Dungeon.h"
#include "LogWindow.h"
#include <ncurses.h>

class Game {
public:
    Game();
    ~Game();
    void run(); // Game loop

private:
    LogWindow m_log_window; // Window on which the text messages are printed
};

#endif // GAME_H
