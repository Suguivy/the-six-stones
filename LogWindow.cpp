#include "LogWindow.h"
#include <ncurses.h>
#include <list>
#include <string>

LogWindow::LogWindow(int x, int y, int width, int height) :
        Window {x, y, width, height},
        m_messages {} {}

LogWindow::LogWindow() :
        Window {},
        m_messages {} {}

LogWindow::~LogWindow() {}

void LogWindow::print(const char* message) {
    m_messages.push_front(message);
}

void LogWindow::draw() {
    // We need to create a new window to draw it
    delwin(m_window);
    m_window = newwin(m_height, m_width, m_y, m_x);
    std::list<const char*>::iterator iter = m_messages.begin();
    // All messages on the message list are printed until they do not fit anymore
    for (int i = m_height - 1; i >= 0 && iter != m_messages.end(); i--, iter++) {
        mvwaddstr(m_window, i, m_x, *iter);
    }
    refresh();
    wrefresh(m_window);
}
