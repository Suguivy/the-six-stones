#ifndef PLAYER_H
#define PLAYER_H


#include "Creature.h"

class Player : public Creature {
public:
    Player(int x, int y);
    ~Player();
    char get_representation(); // Returns the character that represents the player
};

#endif // PLAYER_H
