#ifndef FLOOR_H
#define FLOOR_H

#include "Cell.h"
#include "Creature.h"
#include <vector>
#include <list>

class Floor {
public:
    Floor(int width, int height);
    ~Floor();
    int get_width();
    int get_height();
    Cell& get_cell(int x, int y);
    void redraw_cell(int x, int y);
    bool cell_exists(int x, int y);

private:
    int m_width;
    int m_height;
    std::list<Creature*> m_creatures;
    std::vector<std::vector<Cell>> m_grid;    
};

#endif // FLOOR_H
