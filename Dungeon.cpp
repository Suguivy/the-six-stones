#include "Dungeon.h"
#include "Floor.h"

Dungeon::Dungeon(int total_floors) :
        m_floors {std::vector<Floor> {static_cast<size_t>(total_floors), Floor {30, 30}}},
        m_active_floor {&m_floors[0]},
        m_player {0, 0} {
    // We move the player onto the cell and we redraw that cell
    m_active_floor->get_cell(m_player.get_x(), m_player.get_y()).set_creature(&m_player);
    m_active_floor->redraw_cell(m_player.get_x(), m_player.get_y());
}

Dungeon::~Dungeon() {}

void Dungeon::draw() {
    for (int y = 0; y < m_active_floor->get_height(); y++) {
        for (int x = 0; x < m_active_floor->get_width(); x++) {
            m_active_floor->redraw_cell(x, y);
        }
    }
}

void Dungeon::move_player(int dx, int dy) {
    // If the cell which the player want to move to is free (and exists)
    if (m_active_floor->cell_exists(m_player.get_x() + dx, m_player.get_y() + dy)) {
        // We remove the player of the cell and add it to the new cell, and update the player coordinates
        m_active_floor->get_cell(m_player.get_x(), m_player.get_y()).set_creature(nullptr);
        m_active_floor->redraw_cell(m_player.get_x(), m_player.get_y());
        m_player.move(dx, dy);
        m_active_floor->get_cell(m_player.get_x(), m_player.get_y()).set_creature(&m_player);
        m_active_floor->redraw_cell(m_player.get_x(), m_player.get_y());
    }
}
