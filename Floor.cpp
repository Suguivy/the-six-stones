#include "Floor.h"
#include "Cell.h"
#include <ncurses.h>
#include <vector>

Floor::Floor(int width, int height) :
        m_width {width},
        m_height {height},
        m_creatures {std::list<Creature*> {}},
        // The grid is filled with free cells
        m_grid {std::vector<std::vector<Cell>> {static_cast<size_t>(height), std::vector<Cell> {static_cast<size_t>(width), Cell {CellType::FREE}}}} {}

Floor::~Floor() {}

int Floor::get_width() {
    return m_width;
}

int Floor::get_height() {
    return m_height;
}

Cell& Floor::get_cell(int x, int y) {
    return m_grid[y][x];
}

void Floor::redraw_cell(int x, int y) {
    // The cell representation (char) is printed in its position
    mvaddch(y, x, m_grid[y][x].get_representation());
}

bool Floor::cell_exists(int x, int y) {
    return x >= 0 && x < m_width && y >= 0 && y < m_height;
}
