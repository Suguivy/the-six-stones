#include "Window.h"
#include <ncurses.h>

Window::Window(int x, int y, int width, int height) :
        m_window {newwin(height, width, y, x)},
        m_x {x},
        m_y {y},
        m_width {width},
        m_height {height} {}

Window::Window() :
        m_window {newwin(0, 0, 0, 0)},
        m_x {0},
        m_y {0},
        m_width {0},
        m_height {0} {}

Window::~Window() {
    delwin(m_window);
}

void Window::set_dimensions(int x, int y, int width, int height) {
    m_x = x;
    m_y = y;
    m_width = width;
    m_height = height;
}

void Window::draw() {
    // We need to create a new window to draw it
    delwin(m_window);
    m_window = newwin(m_height, m_width, m_y, m_x);
    box(m_window, 0, 0);
    refresh();
    wrefresh(m_window);
}
