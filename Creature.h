#ifndef CREATURE_H
#define CREATURE_H

class Creature {
public:
    Creature(int x, int y);
    ~Creature();
    int get_x();
    int get_y();
    void move(int dx, int dy);
    virtual char get_representation();

protected:
    int m_x;
    int m_y;
};

#endif // CREATURE_H
