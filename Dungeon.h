#ifndef DUNGEON_H
#define DUNGEON_H

#include "Floor.h"
#include "Player.h"
#include <vector>

class Dungeon {
public:
    Dungeon(int total_floors);
    ~Dungeon();
    void draw();
    void move_player(int dx, int dy);
private:
    std::vector<Floor> m_floors; // All floors that compose the dungeon
    Floor* m_active_floor; // The current active floor
    Player m_player; 
};

#endif // DUNGEON_H
