#include "Creature.h"

Creature::Creature(int x, int y) :
        m_x {x},
        m_y {y} {}

Creature::~Creature() {}

int Creature::get_x() {
    return m_x;
}

int Creature::get_y() {
    return m_y;
}

void Creature::move(int dx, int dy) {
    m_x += dx;
    m_y += dy;
}

char Creature::get_representation() {
    return 'c'; // TODO: changue this with a different char for each creature
}
