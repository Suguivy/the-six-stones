#include "Game.h"
#include "Dungeon.h"
#include <ncurses.h>

Game::Game() {
    initscr(); // Starts ncurses
    noecho(); // Disable character output when pressing a key
    curs_set(0); // Makes cursor invisible
    m_log_window.set_dimensions(0, LINES - 3, COLS, 3);
}

Game::~Game() {
    endwin();
}

void Game::run() {
    int ch;
    
    Dungeon dungeon {10};
    dungeon.draw();
    m_log_window.draw();

    while((ch = getch()) != 'q') {
        switch(ch) {
        case 'h':
            dungeon.move_player(-1, 0);
            m_log_window.print("moving left");
            break;
        case 'j':
            dungeon.move_player(0, 1);
            m_log_window.print("moving down");
            break;
        case 'k':
            dungeon.move_player(0, -1);
            m_log_window.print("moving up");
            break;
        case 'l':
            dungeon.move_player(1, 0);
            m_log_window.print("moving right");
            break;
        }
        m_log_window.draw();
    }
}
