#include "Cell.h"
#include <ncurses.h>

Cell::Cell(CellType type) :
        m_type {type},
        m_creature {nullptr},
        m_items {{}} {}

Cell::~Cell() {}

char Cell::get_representation() {
    // If there are no creature, the representation depends on the type of the cell, else depends on the creature
    if (m_creature == nullptr) {
        switch(m_type) {
        case CellType::SOLID:
            return '#';
        case CellType::FREE:
            return '.';
        default:
            return '?';
        }
    } else {
        return m_creature->get_representation();
    }
}

void Cell::set_creature(Creature* creature) {
    m_creature = creature;
}
