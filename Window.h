#ifndef WINDOW_H
#define WINDOW_H

#include <ncurses.h>

class Window {
public:
    Window(int x, int y, int width, int height);
    Window();
    virtual ~Window();
    void set_dimensions(int x, int y, int width, int height);
    virtual void draw();

protected:
    WINDOW* m_window;
    int m_x;
    int m_y;
    int m_width;
    int m_height;
};

#endif // WINDOW_H
