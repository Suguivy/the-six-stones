#ifndef LOG_WINDOW_H
#define LOG_WINDOW_H

#include "Window.h"
#include <list>
#include <string>

class LogWindow : public Window {
public:
    LogWindow(int x, int y, int width, int height);
    LogWindow();
    ~LogWindow();
    void print(const char* message);
    void draw();

private:
    std::list<const char*> m_messages; // This list stores all the messages printed by the window
};

#endif // LOG_WINDOW_H
